libnetxap-perl (0.02-12) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libnet-imap-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 19 Nov 2022 18:08:58 +0000

libnetxap-perl (0.02-11) unstable; urgency=medium

  * Fix autopkgtests: skip smoke test, like during build (needs an imapd).
  * Add debian/tests/pkg-perl/use-name for autopkgtest's use.t test.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.6.1.
  * Bump debhelper-compat to 13.
  * Drop unversioned 'perl' from Depends.
  * Annotate test-only build dependencies with <!nocheck>.

 -- gregor herrmann <gregoa@debian.org>  Sat, 18 Jun 2022 15:10:28 +0200

libnetxap-perl (0.02-10) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Add missing colon in closes line.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Set Testsuite header for perl package.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 16 Jun 2022 23:12:50 +0100

libnetxap-perl (0.02-9.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Mon, 04 Jan 2021 17:38:40 +0100

libnetxap-perl (0.02-9) unstable; urgency=low

  * Team upload.
  * Install ANNOUNCE, BUGS, CREDITS, README and TODO again as
    documentation. Regression introduced by commit 3142abf6, too.

 -- Axel Beckert <abe@debian.org>  Thu, 09 Nov 2017 22:43:48 +0100

libnetxap-perl (0.02-8) unstable; urgency=low

  * Team upload.

  [ gregor herrmann ]
  * Add debian/README.source to document quilt usage, as required by
    Debian Policy since 3.8.0.
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).
  * Change my email address.

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Fabrizio Regalli ]
  * Bump to 3.9.2 Standard-Version.
  * Switch to DEP5 license format.
  * Add myself to Uploaders.
  * Switch d/compat to 8.
  * Build-Depends: switch to debhelper (>= 8).
  * Bump to 3.0 quilt format.
  * Convert debian/rules to minimal "dh $@" style.
  * Add build-dependencies on courier-imap | uw-imapd.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"
  * Remove obsolete alternative b-d on uw-imapd (gone for good)
  * Add b-d on libdigest-hmac-perl, FTBFS otherwise nowadays
  * Add patch to use /usr/bin/imapd, not /usr/sbin/imapd in test suite

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.
  * Change bugtracker URL(s) to HTTPS.
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Fabrizio Regalli from Uploaders. Thanks for your work!
  * Mark package as autopkgtestable.
  * Remove Jaldhar H. Vyas from Uploaders. Thanks for your work!

  [ Axel Beckert ]
  * Disable test suite for now (never worked, broken since 3142abf6).
    + Drop build-dependency on courier-imap.
    + Drop Testsuite header.
  * Cleanup trailing whitespace in ancient debian/changelog entries.
  * Declare compliance with Debian Policy 4.1.1.
  * Bump debhelper compatibility level to 10.
    + Update versioned debhelper build-dependency accordingly.
  * Drop article from short package description.
  * Install file "NEWS" as upstream changelog.
  * Mention additionally supported protocols in long package description.
  * Add patch to fix spelling errors found by lintian.
  * Extend 20pod.patch to fix missing quoting around "*" in "=item *"
    where is wasn't mean to make a dotted list of items.
  * Set "Rules-Requires-Root: no".

 -- Axel Beckert <abe@debian.org>  Thu, 09 Nov 2017 22:22:56 +0100

libnetxap-perl (0.02-7) unstable; urgency=low

  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Removed: XS-
    Vcs-Svn fields. Added: /me to Uploaders.
  * debian/watch: use dist-based URL.
  * debian/rules:
    - delete /usr/lib/perl5 only if it exists (closes: #467989)
    - update based on dh-make-perl's templates
  * Delete debian/examples, install sample scripts directly from
    debian/rules.
  * Set Standards-Version to 3.7.3 (no changes).
  * debian/copyright: add download URL.
  * Change patch system from dpatch to quilt.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Sun, 09 Mar 2008 16:00:14 +0100

libnetxap-perl (0.02-6) unstable; urgency=low

  * Migrate to dpatch.
  * Upgrade to Standards-Version 3.7.2. No changes needed.
  * Upgrade to debhelper compatibility level 5.
  * Move debhelper from Build-Depends-Indep to Build-Depends,
    as it's required by the 'clean' target.
  * Don't ignore the return value of 'make realclean'.
  * Cleanup of debian/rules.
  * debian/patches:
    + 20pod.dpatch: One more fix for POD comments.
    + 60lc.dpatch: Fix typo in Net::IMAP:Acl::new()
    + 70search.dpatch: Fix 'use of uninitialized value' messages
      when an IMAP search doesn't match.
    + 80expunge.dpatch: Fix handling of expunge responses.
      Thanks to Torsten Hilbrich for the patch. (Closes: #366209)
    + 90quota.dpatch: Fix quota support.
      Thanks to Mike Beattie for the patch. (Closes: #366480)
  * Don't install an empty '/usr/lib/perl5' directory.

 -- Niko Tyni <ntyni@iki.fi>  Sat, 13 May 2006 22:00:04 +0300

libnetxap-perl (0.02-5) unstable; urgency=low

  * Suppress debug output unless we actually asked for it.  Thanks to
    Morten Bgeskov for the patch.  (Closes: #319558)

 -- Jaldhar H. Vyas <jaldhar@debian.org>  Wed,  3 Aug 2005 17:44:55 -0400

libnetxap-perl (0.02-4) unstable; urgency=low

  * Package now maintained by the Debian Perl group.

 -- Jaldhar H. Vyas <jaldhar@debian.org>  Wed, 15 Jun 2005 09:32:11 -0400

libnetxap-perl (0.02-3) unstable; urgency=low

  * Fixed mishandling of IMAP banner (Closes: #256836)
  * I think I fixed the issue with the namespace function returning NIL.
    Further testing would be helpful.  (Closes: #256837)

 -- Jaldhar H. Vyas <jaldhar@debian.org>  Wed,  7 Jul 2004 22:13:09 -0400

libnetxap-perl (0.02-2) unstable; urgency=low

  * Added missing dependency on libdigest-hmac-perl.  Removed incorrect
    dependency on libmd4-perl (Closes: #221017)

 -- Jaldhar H. Vyas <jaldhar@debian.org>  Sun, 16 Nov 2003 08:30:42 -0500

libnetxap-perl (0.02-1) unstable; urgency=low

  * Initial Release (Closes: #204112.)

 -- Jaldhar H. Vyas <jaldhar@debian.org>  Mon,  4 Aug 2003 13:28:16 -0400
